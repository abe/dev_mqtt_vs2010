Most recent updates appear at the top of this file.

KNOWN BUGS
==========
<NONE>

version 002 / 000
-----------------
-DEPENDENCIES: MultiCODE v541.045, MQTT-C ($MultiCODE_DIR)paho.mqtt.c_VS2010\install)
-ADDED: UniCS_derot_server project.


version 001 / 000
-----------------
-DEPENDENCIES: MultiCODE v540.045, MQTT-C ($MultiCODE_DIR)paho.mqtt.c_VS2010\install)
-UPDATED: working code tested with minor updates.


version 000 / 000
-----------------
-DEPENDENCIES: MultiCODE v540.045, MQTT-C ($MultiCODE_DIR)paho.mqtt.c_VS2010\install)
