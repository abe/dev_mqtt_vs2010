#include <cstdlib>
#include <cstdio>
#include <string>

#include <MultiCODE.h>
#include <MCODE.h>
#include <MCODE_MotController.h>
#include <MCODE_MotController_Newport.h>

#include <MQTTAsync.h>

#if defined(WIN64)
	#if defined(_DEBUG)
		#pragma comment(lib,"Ws2_32.Lib")
		#pragma comment(lib,"libcpuid_x64_D.lib")
		#pragma comment(lib,"paho-mqtt3a_x64.lib")
		#pragma comment(lib,"paho-mqtt3c_x64.lib")
		//#pragma comment(linker, "/nodefaultlib:\"mfc100ud.lib\"")
		//#pragma comment(linker, "/nodefaultlib:\"msvcprtd.lib\"")
		//#pragma comment(linker, "/nodefaultlib:\"msvcp100d.lib\"")
		#pragma comment(linker, "/nodefaultlib:\"libcpmtd.lib\"")
		#pragma comment(linker, "/nodefaultlib:\"libcmtd.lib\"")
		#pragma comment(lib,"MCODE_LogManager_x64_D.lib")
		#pragma comment(lib,"MCODE_Module_x64_D.lib")
		#pragma comment(lib,"MCODE_MiscUtilities_x64_D.lib")
		#pragma comment(lib,"MCODE_MotController_Newport_x64_D.lib")
		#pragma comment(lib,"MCODE_StdFunc_x64_D.lib")
		#pragma comment(lib,"MCODE_SystemUtilities_x64_D.lib")
	#else
		#pragma comment(lib,"MCODE_MiscUtilities_x64.lib")
	#endif
#elif defined(WIN32)

#endif

using namespace std;
using namespace MultiCODE;

static string APP_name = "UniCS_derot_server";
static string APP_revision = "001";

string								LogMsg;

MCODE_LogManager					*Log = NULL;
MCODE_MotController_Newport			*XPS = NULL;

int main(int argc, char* argv[])
{
	string msg = string_format("%s rev%s\n\n",APP_name.data(),APP_revision.data());
	cout << msg;

	if (argc != 5)
	{
		msg = string_format("Invalid number of paramaters (%d provided, 4 needed). Quitting.\n",argc);
		cout << msg;
		system("pause");
		return -1;
	}

	string m_AddressAndPort =		(char*)argv[1];
	string m_ClientID =				(char*)argv[2];
	string m_Username = 			(char*)argv[3];
	string m_Password = 			(char*)argv[4];

	msg = string_format("Address:   %s\n"
						"ID:        %s\n"
						"User:      %s\n"
						"Password:  %s\n"
						"\n",
						m_AddressAndPort.data(),m_ClientID.data(),m_Username.data(),m_Password.data());
	cout << msg;

	Log = new MCODE_LogManager(string_format("C:\\Home\\%s.log",APP_name.data()),true);
	
	if (Log == NULL)
	{
		LogMsg = string_format("[%s] ERROR: could not create 'Log' instance. Quitting.",__FUNCTION__);
		Log->StackAddMsg(LOG_SERIOUS,0,__LINE__,__FILE__,LogMsg);
		Log->Stop();
		delete Log;
		return -1;
	}

	Log->Start();

	XPS = new MCODE_MotController_Newport(Log,"Newport XPS-D");

	if (XPS == NULL)
	{
		LogMsg = string_format("[%s] ERROR: could not create 'XPS' instance. Quitting.",__FUNCTION__);
		Log->StackAddMsg(LOG_SERIOUS,0,__LINE__,__FILE__,LogMsg);
		Log->Stop();
		delete Log;
		return -1;
	}

	Log->Stop();
	delete Log;
	Log = NULL;

	system("pause");

	return 0;
}